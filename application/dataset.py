import os
import uuid
from dateutil.parser import parse

import numpy as np
import pandas as pd
from flask import request, jsonify, g
from werkzeug import secure_filename

from index import app, db
from .utils.auth import requires_auth
from .models import Dataset, Column


@app.route('/api/user/datasets', methods=['POST'])
@requires_auth
def create_dataset():
    user = g.current_user
    if 'file' in request.files:
        file = request.files['file']
        filename, extension = os.path.splitext(file.filename)
        filename = "{fn}.{id}{ext}".format(fn=filename,
                        id=uuid.uuid4(), ext=extension)
        filename = secure_filename(filename)
        full_filename = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        file.save(full_filename)

        try:
            df = pd.read_csv(full_filename)
        except Exception as e:
            os.remove(full_filename)
            return jsonify(message=e.message), 400

        for col in df.columns:
            try:
                if df[col].dtype == object and df.shape[0] > 0 and parse(str(df[col].iloc[0])):
                    df[col] = pd.to_datetime(df[col])
            except ValueError:
                pass

        row_num, col_num = df.shape
        new_dataset = Dataset(owner_id=user["id"], filename=filename, row_num=row_num, col_num=col_num)
        db.session.add(new_dataset)

        cols_group = [[np.number], ['category', object], ['datetime']]
        dtypes = ['numerical', 'categorical', 'datetime']
        for cols,dtype in zip(cols_group, dtypes):
            dcols = df.select_dtypes(include=cols).columns
            new_cols = [Column(dataset=new_dataset, name=col.decode('utf-8'), dtype=dtype)
                        for col in dcols]
            db.session.add_all(new_cols)
        db.session.commit()
        return jsonify(filename=filename), 201
    else:
        return jsonify(message='missing upload file'), 400


@app.route('/api/user/datasets', methods=['GET'])
@requires_auth
def read_all_datasets():
    user = g.current_user
    output = list()
    for row in Dataset.query.filter_by(owner_id=user['id']).all():
        columns = list({'name': col.name, 'type': col.dtype }
                        for col in Column.query.filter_by(dataset_id=row.id))
        output.append({'filename': row.filename, 'created_at': row.created_at, 'id': row.id,
                       'row_num': row.row_num, 'col_num': row.col_num, 'columns': columns})
    return jsonify(output)


@app.route('/api/user/datasets/<int:dataset_id>', methods=['GET'])
@requires_auth
def read_dataset(dataset_id):
    user = g.current_user
    dataset = Dataset.query.filter_by(id=dataset_id, owner_id=user['id']).first_or_404()
    full_filename = os.path.join(app.config['UPLOAD_FOLDER'], dataset.filename)
    try:
        df = pd.read_csv(full_filename)
    except Exception as e:
        return jsonify(message=e.message), 400
    return df.to_json(orient='records')


@app.route('/api/user/datasets/<int:dataset_id>', methods=['DELETE'])
@requires_auth
def delete_dataset(dataset_id):
    user = g.current_user
    dataset = Dataset.query.filter_by(owner_id=user['id'], id=dataset_id).first_or_404()
    os.remove(os.path.join(app.config['UPLOAD_FOLDER'], dataset.filename))
    db.session.delete(dataset)
    db.session.commit()
    return jsonify(result=True)
