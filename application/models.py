from datetime import datetime

from index import db, bcrypt


class User(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))

    def __init__(self, email, password):
        self.email = email
        self.active = True
        self.password = User.hashed_password(password)

    @staticmethod
    def hashed_password(password):
        return bcrypt.generate_password_hash(password)

    @staticmethod
    def get_user_with_email_and_password(email, password):
        user = User.query.filter_by(email=email).first()
        if user and bcrypt.check_password_hash(user.password, password):
            return user
        else:
            return None


class Dataset(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    owner_id = db.Column(db.Integer(), db.ForeignKey('user.id', ondelete='CASCADE'))
    filename = db.Column(db.String(), default=None, nullable=True)
    created_at = db.Column(db.DateTime(), default=datetime.utcnow)
    row_num = db.Column(db.Integer(), nullable=False)
    col_num = db.Column(db.Integer(), nullable=False)
    user = db.relationship(User, backref=db.backref('dataset', uselist=True, cascade='delete,all'))


class Column(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    dataset_id = db.Column(db.Integer(), db.ForeignKey('dataset.id', ondelete='CASCADE'))
    name = db.Column(db.String(), nullable=False)
    dtype = db.Column(db.String(), nullable=False)
    dataset = db.relationship(Dataset, backref=db.backref('column', uselist=True, cascade='delete,all'))
