# Visualization Engine

This project is built using [React-Redux-Flask](https://github.com/dternyak/React-Redux-Flask.git) with D3 library.

### Setup Virtual Environment & Create DB

```sh
$ virtualenv .venv
$ source .venv/bin/activate
$ pip install -r requirements.txt

$ python manage.py create_db
```

### Install Front-End Requirements

```sh
$ cd static
$ npm install
```

### Run Back-End

```sh
$ python manage.py runserver
```

### Run Front-End

```sh
$ cd static
$ npm start
```
