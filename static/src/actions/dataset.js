import { FETCH_PROTECTED_DATASETS_REQUEST,
         FETCH_PROTECTED_DATASETS_SUCCESS,
         FETCH_PROTECTED_DATASETS_FAILURE,
         FETCH_PROTECTED_DATASET_REQUEST,
         FETCH_PROTECTED_DATASET_SUCCESS,
         FETCH_PROTECTED_DATASET_FAILURE,
         DELETE_PROTECTED_DATASET_REQUEST,
         DELETE_PROTECTED_DATASET_SUCCESS,
         DELETE_PROTECTED_DATASET_FAILURE,
         CREATE_PROTECTED_DATASET_REQUEST,
         CREATE_PROTECTED_DATASET_SUCCESS,
         CREATE_PROTECTED_DATASET_FAILURE,
         SET_FILTERED_DATASET,
         SET_FILTER } from '../constants';
import { fetch_datasets, fetch_dataset, delete_dataset, upload_dataset } from '../utils/http_functions';


export function fetchProtectedDatasets(token) {
    return {
        type: FETCH_PROTECTED_DATASETS_REQUEST,
        payload: fetch_datasets(token)
    };
}

export function fetchProtectedDatasetsSuccess(datasets) {
    return {
        type: FETCH_PROTECTED_DATASETS_SUCCESS,
        payload: datasets
    };
}

export function fetchProtectedDatasetsFailure(error) {
    return {
        type: FETCH_PROTECTED_DATASETS_FAILURE,
        payload: error
    };
}

export function deleteProtectedDataset(token, id) {
    return {
        type: DELETE_PROTECTED_DATASET_REQUEST,
        payload: delete_dataset(token, id)
    };
}

export function deleteProtectedDatasetSuccess(result) {
    return {
        type: DELETE_PROTECTED_DATASET_SUCCESS,
        payload: result
    };
}

export function deleteProtectedDatasetFailure(error) {
    return {
        type: DELETE_PROTECTED_DATASET_FAILURE,
        payload: error
    };
}

export function createProtectedDataset(token, file) {
    return {
        type: CREATE_PROTECTED_DATASET_REQUEST,
        payload: upload_dataset(token, file)
    }
}

export function createProtectedDatasetSuccess(result) {
    return {
        type: CREATE_PROTECTED_DATASET_SUCCESS,
        payload: result
    }
}

export function createProtectedDatasetFailure(error) {
    return {
        type: CREATE_PROTECTED_DATASET_FAILURE,
        payload: error
    }
}

export function fetchProtectedDataset(token, dataset_id) {
    return {
        type: FETCH_PROTECTED_DATASET_REQUEST,
        payload: fetch_dataset(token, dataset_id)
    };
}

export function fetchProtectedDatasetSuccess(dataset) {
    return {
        type: FETCH_PROTECTED_DATASET_SUCCESS,
        payload: dataset
    };
}

export function fetchProtectedDatasetFailure(error) {
    return {
        type: FETCH_PROTECTED_DATASET_FAILURE,
        payload: error
    };
}

export function setFilteredDataset(rawData) {
    return {
        type: SET_FILTERED_DATASET,
        payload: rawData
    };
}

export function setFilter(filter) {
    return {
        type: SET_FILTER,
        payload: filter
    };
}
