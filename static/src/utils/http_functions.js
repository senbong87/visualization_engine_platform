/* eslint camelcase: 0 */

import axios from 'axios';

const tokenConfig = (token) => ({
    headers: {
        'Authorization': token, // eslint-disable-line quote-props
    },
});

export function validate_token(token) {
    return axios.post('/api/is_token_valid', {
        token,
    });
}

export function get_github_access() {
    window.open(
        '/github-login',
        '_blank' // <- This is what makes it open in a new window.
    );
}

export function create_user(email, password) {
    return axios.post('api/create_user', {
        email,
        password,
    });
}

export function get_token(email, password) {
    return axios.post('api/get_token', {
        email,
        password,
    });
}

export function has_github_token(token) {
    return axios.get('api/has_github_token', tokenConfig(token));
}

export function data_about_user(token) {
    return axios.get('api/user', tokenConfig(token));
}

export function fetch_datasets(token) {
    return axios.get('api/user/datasets', tokenConfig(token));
}

export function delete_dataset(token, dataset_id) {
    return axios.delete('api/user/datasets/' + dataset_id, tokenConfig(token));
}

export function upload_dataset(token, file) {
    let config = {
        headers: {
            'Authorization': token, // eslint-disable-line quote-props
            'Content-Type': 'multipart/form-data',  // eslint-disable-line quote-props
        },
    };
    let form_data = new FormData();
    form_data.append("file", file);
    return axios.post('api/user/datasets', form_data, config);
}

export function fetch_dataset(token, dataset_id) {
    return axios.get('api/user/datasets/' + dataset_id, tokenConfig(token));
}
