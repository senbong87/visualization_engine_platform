/* eslint max-len: 0, no-param-reassign: 0 */

export function createConstants(...constants) {
    return constants.reduce((acc, constant) => {
        acc[constant] = constant;
        return acc;
    }, {});
}

export function createReducer(initialState, reducerMap) {
    return (state = initialState, action) => {
        const reducer = reducerMap[action.type];


        return reducer
            ? reducer(state, action.payload)
            : state;
    };
}


export function parseJSON(response) {
    return response.data;
}

export function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

export function trunc(s, n) {
    return s.length > n ? s.substr(0, n-1) + '...' : s;
}

export function jsonListToColList(data) {
    let output = {};
    data.forEach(function (row) {
        for(let key in row) {
            if(key in output) {
                output[key].push(row[key]);
            } else {
                output[key] = [row[key]];
            }
        }
    });
    return output;
}

export function jitter(band) {
    return (Math.random() - 0.5) * band;
}
