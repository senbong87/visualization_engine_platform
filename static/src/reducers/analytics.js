import { LOAD_PLOT_REQUEST } from '../constants';
import { createReducer } from '../utils/misc';

const initialState = {
    plotType: null,
};

export default createReducer(initialState, {
    [LOAD_PLOT_REQUEST]: (state, plot_type) =>
        Object.assign({}, state, {
            plotType: plot_type,
        }),
});
