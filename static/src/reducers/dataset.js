import { FETCH_PROTECTED_DATASETS_REQUEST,
         FETCH_PROTECTED_DATASETS_SUCCESS,
         FETCH_PROTECTED_DATASETS_FAILURE,
         FETCH_PROTECTED_DATASET_REQUEST,
         FETCH_PROTECTED_DATASET_SUCCESS,
         FETCH_PROTECTED_DATASET_FAILURE,
         DELETE_PROTECTED_DATASET_REQUEST,
         DELETE_PROTECTED_DATASET_SUCCESS,
         DELETE_PROTECTED_DATASET_FAILURE,
         CREATE_PROTECTED_DATASET_REQUEST,
         CREATE_PROTECTED_DATASET_SUCCESS,
         CREATE_PROTECTED_DATASET_FAILURE,
         SELECT_PROTECTED_DATASET,
         SELECT_DATASET_COLUMN,
         SET_FILTERED_DATASET,
         SET_FILTER } from '../constants';
import { createReducer } from '../utils/misc';

const initialState = {
    datasetsList: { datasets: [], error: null, loading: false },
    newDataset: { dataset: null, error: null, loading: false },
    deletedDataset: { result: null, error: null, loading: false },
    activeDataset: { rawData: [], selectedColumns: [], meta: null, error: null, loading: false, filter: null },
    filteredDataset: { rawData: [], error: null, loading: false },
};

export default createReducer(initialState, {
    [FETCH_PROTECTED_DATASETS_REQUEST]: (state) =>
        Object.assign({}, state, {
            datasetsList: { datasets: [], error: null, loading: true }
        }),
    [FETCH_PROTECTED_DATASETS_SUCCESS]: (state, datasets) =>
        Object.assign({}, state, {
            datasetsList: { datasets: datasets, loading: false }
        }),
    [FETCH_PROTECTED_DATASETS_FAILURE]: (state, error) =>
        Object.assign({}, state, {
            datasetsList: { datasets: [], error: error, loading: false }
        }),
    [FETCH_PROTECTED_DATASET_REQUEST]: (state) =>
        Object.assign({}, state, {
            activeDataset: { ...state.activeDataset, loading: true }
        }),
    [FETCH_PROTECTED_DATASET_SUCCESS]: (state, dataset) => {
            // access meta `state.activeDataset.meta`
            // create a new dataset from `dataset` with desired format
            let function_map = Object();
            state.activeDataset.meta.columns.forEach((col_meta) => {
                if(col_meta["type"] == "datetime") {
                    function_map[col_meta["name"]] = (x) => (new Date(x));     
                } else {
                    function_map[col_meta["name"]] = (x) => x;
                }

            });
            let dataset_jsDate = dataset.map((row) => {
                return Object.keys(row).reduce((acc, x) => {
                    acc[x] = function_map[x](row[x]);
                    return acc;
                }, {});
            });
            return Object.assign({}, state, {
                activeDataset: { ...state.activeDataset, rawData: dataset_jsDate, loading: false },
                filteredDataset: { ...state.filteredDataset, rawData: dataset_jsDate, loading: false }
            })
        },
    [FETCH_PROTECTED_DATASET_FAILURE]: (state, error) =>
        Object.assign({}, state, {
            activeDataset: { ...state.activeDataset, error: error, loading: false }
        }),
    [DELETE_PROTECTED_DATASET_REQUEST]: (state) =>
        Object.assign({}, state, {
            deletedDataset: { ...state.deletedDataset, loading: true }
        }),
    [DELETE_PROTECTED_DATASET_SUCCESS]: (state, result) =>
        Object.assign({}, state, {
            deletedDataset: { result: result, error: null, loading: false }
        }),
    [DELETE_PROTECTED_DATASET_FAILURE]: (state, error) =>
        Object.assign({}, state, {
            deletedDataset: { result: null, error: error, loading: false }
        }),
    [CREATE_PROTECTED_DATASET_REQUEST]: (state) =>
        Object.assign({}, state, {
            newDataset: { ...state.newDataset, loading:  true }
        }),
    [CREATE_PROTECTED_DATASET_SUCCESS]: (state, dataset) =>
        Object.assign({}, state, {
            newDataset: {}
        }),
    [CREATE_PROTECTED_DATASET_FAILURE]: (state, error) =>
        Object.assign({}, state, {
            newDataset: { dataset: null, error: error, loading: false }
        }),
    [SELECT_PROTECTED_DATASET]: (state, meta) =>
        Object.assign({}, state, {
            activeDataset: { ...state.activeDataset, selectedColumns: [], meta: meta }
        }),
    [SELECT_DATASET_COLUMN]: (state, columns) =>
        Object.assign({}, state, {
            activeDataset: { ...state.activeDataset, selectedColumns: columns }
        }),
    [SET_FILTERED_DATASET]: (state, filteredDataset) =>
        Object.assign({}, state, {
            filteredDataset: { ...state.filteredDataset, rawData: filteredDataset }
        }),
    [SET_FILTER]: (state, filter) =>
        Object.assign({}, state, {
            activeDataset: { ...state.activeDataset, filter: filter }
        }),
});
