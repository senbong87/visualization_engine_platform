import React from 'react';
import PropTypes from 'prop-types';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import StickyBox from "react-sticky-box";
import * as authActions from '../actions/auth';
import * as datasetActions from '../actions/dataset';
import Toolbar from './Toolbar';
import PlotBoard from './PlotBoard';
import { SELECT_PROTECTED_DATASET,
         SELECT_DATASET_COLUMN,
         LOAD_PLOT_REQUEST } from '../constants';

function mapStateToProps(state) {
    return {
        token: state.auth.token,
        isRegistering: state.auth.isRegistering,
        registerStatusText: state.auth.registerStatusText,
        datasetsList: state.dataset.datasetsList,
        activeDataset: state.dataset.activeDataset,
        plotType: state.analytics.plotType,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Object.assign({}, authActions, datasetActions, { dispatch: dispatch }), dispatch);
}

function mergeProps(stateProps, dispatchProps) {
    return {
        ...stateProps,
        ...dispatchProps,
        onDatasetSelectFieldChange: (evt, index, value) => {
            const datasets = stateProps.datasetsList.datasets.filter((dataset) => {
                return dataset.id === value;
            });
            dispatchProps.dispatch({
                type: SELECT_PROTECTED_DATASET,
                payload: datasets[0]
            });

            const token = stateProps.token;
            dispatchProps.fetchProtectedDataset(token, datasets[0].id).payload.then(
                (response) => {
                    !response.error
                        ? dispatchProps.fetchProtectedDatasetSuccess(response.data)
                        : dispatchProps.fetchProtectedDatasetFailure(response.error)
                }
            );
        },
        onColumnSelectFieldChange: (evt, index, values) => {
            dispatchProps.dispatch({
                type: SELECT_DATASET_COLUMN,
                payload: values
            });
        },
        onPlotBoardDrop: (plot_type_item) => {
            const plot_type = plot_type_item === null ? null : plot_type_item.name;
            dispatchProps.dispatch({
                type: LOAD_PLOT_REQUEST,
                payload: plot_type
            });
        }
    }
}

@DragDropContext(HTML5Backend)
@connect(mapStateToProps, mapDispatchToProps, mergeProps)
class Analytics extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
        onPlotBoardDrop: PropTypes.func.isRequired,
        onDatasetSelectFieldChange: PropTypes.func.isRequired,
        onColumnSelectFieldChange: PropTypes.func.isRequired,
        plotType: PropTypes.string,
    };

    getPlotTypeColumn() {
        if(this.props.activeDataset === null || this.props.activeDataset.meta === null) {
            return [];
        } else if(this.props.plotType === null){
            return this.props.activeDataset.meta.columns;
        } else {
            let num_cols = this.props.activeDataset.meta.columns.filter(x => x.type === 'numerical');
            let cat_cols = this.props.activeDataset.meta.columns.filter(x => x.type === 'categorical');
            let date_cols = this.props.activeDataset.meta.columns.filter(x => x.type === 'datetime');
            let type_map = {
                dotplot: num_cols,
                parallel_coordinate: num_cols.concat(cat_cols),
                time_series: date_cols.concat(num_cols),
                dotplot_timebrush: date_cols.concat(num_cols),
                starplot: date_cols.concat(num_cols),
                geomap: date_cols.concat(cat_cols),
            };
            return type_map[this.props.plotType];
        }
    }

    render() {
        const datasetMenuItems = this.props.datasetsList.datasets.map((dataset) => (
            <MenuItem key={dataset.id} value={dataset.id} primaryText={dataset.filename} />
        ));

        const columns = this.getPlotTypeColumn();
        const columnMenuItems = function(values) {
            return columns.map((column) => (
                <MenuItem key={column.name} value={column.name} primaryText={column.name}
                          insetChildren={true} checked={values && values.indexOf(column.name) > -1} />
            ));
        }

        return (
            <div className="col-md-12">
                <h1>Analytics
                    <SelectField floatingLabelText="Column"
                        className="pull-right"
                        multiple={true}
                        value={this.props.activeDataset.meta === null ? null : this.props.activeDataset.selectedColumns}
                        onChange={this.props.onColumnSelectFieldChange}>
                        {columnMenuItems(columns)}
                    </SelectField>
                    <SelectField floatingLabelText="Dataset"
                        className="pull-right"
                        value={this.props.activeDataset.meta === null ?  null : this.props.activeDataset.meta.id}
                        onChange={this.props.onDatasetSelectFieldChange}>
                        {datasetMenuItems}
                    </SelectField>
                </h1>
                <br />
                <div className="row">
                    <div className="col-md-10 col-sm-9">
                        <PlotBoard onDrop={this.props.onPlotBoardDrop} plotType={this.props.plotType} />
                    </div>
                    <StickyBox bottom={false} width="measure">
                        <Toolbar className="col-md-2 col-sm-3" />
                    </StickyBox>
                </div>
            </div>
        );
    }
}

export default Analytics;
