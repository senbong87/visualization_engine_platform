import React from 'react';
import PropTypes from 'prop-types';
import { DragSource } from 'react-dnd';
import { ITEMTYPES } from '../../constants';

const cardSource = {
    beginDrag(props) {
        return { name: props.name };
    }
};

function collect(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging()
    }
}

@DragSource(ITEMTYPES.CARD, cardSource, collect)
class Card extends React.Component {
    static propTypes = {
        connectDragSource: PropTypes.func.isRequired,
        isDragging: PropTypes.bool.isRequired
    };

    render() {
        const { connectDragSource, isDragging } = this.props;
        return connectDragSource(<div style={{ opacity: isDragging ? 0.5: 1 }}>{this.props.name}</div>);
    }
}

class Toolbar extends React.Component {
    render() {
        return (
            <div>
                <Card name="dotplot" />
                <Card name="parallel_coordinate" />
                <Card name="time_series" />
                <Card name="dotplot_timebrush" />
                <Card name="starplot" />
                <Card name="geomap" />
            </div>
        );
    }
}

export default Toolbar;
