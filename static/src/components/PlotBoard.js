import React from 'react';
import PropTypes from 'prop-types';
import { DropTarget } from 'react-dnd';
import { ITEMTYPES } from '../constants';
import Parcoord from './chart/Parcoord';
import Dotplot from './chart/Dotplot';
import TimeSeries from './chart/TimeSeries';
import DotplotTimebrush from './chart/DotplotTimebrush';
import Starplot from './chart/Starplot';
import Geomap from './chart/Geomap';

const cardTarget = {
    drop(props, monitor) {
        props.onDrop(monitor.getItem());
    }
};

function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver(),
    };
}

@DropTarget(ITEMTYPES.CARD, cardTarget, collect)
class PlotBoard extends React.Component {
    static propTypes = {
        onDrop: PropTypes.func.isRequired,
        plotType: PropTypes.string,
    };

    render() {
        const { connectDropTarget, isOver, plotType } = this.props;
        return connectDropTarget(
            <div style={{
                backgroundColor: 'whitesmoke',
                width: '100%',
                overflow: 'auto',
                minHeight: '700px'
            }}>
                {{
                     dotplot: <Dotplot />,
                     parallel_coordinate: <Parcoord />,
                     time_series: <TimeSeries />,
                     dotplot_timebrush: <DotplotTimebrush />,
                     starplot: <Starplot />,
                     geomap: <Geomap />,
                 }[this.props.plotType]}
            </div>
        );
    }
}

export default PlotBoard;
