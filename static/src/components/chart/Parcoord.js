import React from 'react';
import { connect } from 'react-redux';
import ContainerDimensions from 'react-container-dimensions';
import { bindActionCreators } from 'redux';
import * as d3 from 'd3';
import * as authActions from '../../actions/auth';
import * as datasetActions from '../../actions/dataset';
import { jsonListToColList, jitter } from '../../utils/misc';
import { Loader } from '../common';
import PAxis from './geometry/PAxis';

function mapStateToProps(state) {
    return {
        token: state.auth.token,
        activeDataset: state.dataset.activeDataset,
        filteredDataset: state.dataset.filteredDataset,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Object.assign({}, authActions, datasetActions), dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
class Parcoord extends React.Component {
    render() {
        return (
            <div>
                <ContainerDimensions>
                    { 
                        ({ width, height }) => {                            
                            const margin = {top: 30, right: 50, bottom: 50, left:10};
                            const innerWidth = width - margin.left - margin.right;
                            const innerHeight = width - margin.top - margin.bottom;
                            const fdata = this.props.activeDataset.rawData;
                            const data = this.props.filteredDataset.rawData;
                            let x = d3.scalePoint().range([0, innerWidth ]).padding(.1),
                                y = {};
                            let line = d3.line();
                            this.props.activeDataset.selectedColumns.forEach((key) => {                                 
                                switch (this.props.activeDataset.meta.columns.find((x) => {return x.name == key}).type) {
                                    case "numerical":
                                        y[key] = d3.scaleLinear()
                                                  .domain(d3.extent(fdata, function(p) { return +p[key]; }))
                                                  .range([innerHeight + margin.top, margin.top]);
                                        break;
                                    case "datetime":
                                        y[key] = d3.scaleTime()
                                                  .domain(d3.extent(fdata, function(p) { return p[key]; }))
                                                  .range([innerHeight + margin.top, margin.top]);
                                        break;
                                    case "categorical":
                                        y[key] = d3.scalePoint()
                                                  .domain(fdata.map((p) => p[key]))
                                                  .range([innerHeight + margin.top, margin.top]);
                                        break;
                                    default:
                                        alert("Column(s) data type cannot be recognized.")

                                }
                            });                            
                            x.domain(this.props.activeDataset.selectedColumns);

                            // Returns the path for a given data point.
                            function path(d, columns) {
                              return line(columns.map(function(p) { return [x(p), y[p](d[p])]; }));
                            }
                            
                            let paths = data.map((val, idx) => <path key={idx} d={path(val, this.props.activeDataset.selectedColumns)} fill={"none"} stroke={"steelblue"} />)

                            let brushes = this.props.activeDataset.selectedColumns.reduce((acc, val) => {
                    					let brush = d3.brushY()
                                            .extent([[0, 0], [10, innerHeight]])                                                            
                                            .on("brush", () => {                                                
                                                let s = d3.event.selection;
                                                let s_inv = s.map(y[val].invert);
                                                let filters = Object.assign({}, this.props.activeDataset.filter);                                                
                                                filters[val] = s_inv;                                                
                                                let filtered_output = Object.keys(filters).reduce((facc, x) => {
                                                        facc = facc.filter((valu) => {
                                                        return (+valu[x] <= +filters[x][0] && +valu[x] >= +filters[x][1]);
                                                    });
                                                    return facc;
                                                }, fdata);
                                                // console.log(filtered_output);
                                                this.props.setFilteredDataset(filtered_output);
                                                this.props.setFilter(filters);
                                            });
                                          acc[val] = brush	
                                return acc
                            }, {});

                            let axes = this.props.activeDataset.selectedColumns.map((val, idx) => <PAxis key={idx}
                                                                                                         yScale={y[val]} 
                                                                                                         xloc={x(val)}
                                                                                                         yloc={margin.top}
                                                                                                         label={val}
                                                                                                         brush={brushes[val]}
                                                                                                         dim={val} />)

                            if(this.props.activeDataset.loading) {
                                return <Loader />;
                            } else {
                                return (
                                    <svg width={width} height={width}>
                                    <g transform={"translate(" + margin.top + ", " +  margin.left + ")"}>
                                        {axes}
                                        {paths}
                                     </g>
                                    </svg>);
                            }
                        }
                    }
                </ContainerDimensions>
            </div>
        );
    }
}

export default Parcoord;
