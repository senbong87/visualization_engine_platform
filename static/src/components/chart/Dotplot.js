import React from 'react';
import { connect } from 'react-redux';
import ContainerDimensions from 'react-container-dimensions';
import { bindActionCreators } from 'redux';
import * as d3 from 'd3';
import * as authActions from '../../actions/auth';
import * as datasetActions from '../../actions/dataset';
import { jsonListToColList, jitter } from '../../utils/misc';
import { Loader } from '../common';
import XYAxis from './geometry/XYAxis';

function mapStateToProps(state) {
    return {
        activeDataset: state.dataset.activeDataset,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Object.assign({}, authActions, datasetActions), dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
class Dotplot extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <ContainerDimensions>
                    { 
                        ({ width, height }) => {
                            if (this.props.activeDataset.rawData.length===0) {
                                return (<p>Please select a dataset.</p>)
                            }
                            const max_chars = this.props.activeDataset.selectedColumns.reduce(
                                                    (acc, x) => Math.max(acc, x.length), 8)
                            const padding = Math.min(max_chars*8, width/2.5);
                            const data = jsonListToColList(this.props.activeDataset.rawData);
                            let numColumns = [];
                            const datetimeColNames = this.props.activeDataset.meta.columns.filter(x => x.type === 'datetime').map(x => x.name);
                            this.props.activeDataset.selectedColumns.forEach((key) => {
                                if (datetimeColNames.indexOf(key)<0) {
                                    if(data[key].every(x => !isNaN(x))) {
                                        numColumns.push(key);
                                    }
                                }
                            });

                            let minVal = d3.min(numColumns.map(key => d3.min(data[key])));
                            let maxVal = d3.max(numColumns.map(key => d3.max(data[key])));
                            minVal = typeof minVal !== 'undefined' ? minVal: 0;
                            maxVal = typeof maxVal !== 'undefined' ? maxVal: 1;
                            const widthScale = d3.scaleLinear()
                                                 .range([padding, width - padding])
                                                 .domain([minVal, maxVal]);
                            const heightScale = d3.scaleBand()
                                                  .range([padding, height - padding])
                                                  .domain(numColumns);
                            const color = d3.scaleOrdinal(d3.schemeCategory10);
                            const band = heightScale.bandwidth();
                            let circles = [];
                            numColumns.forEach((key, kidx) => {
                                data[key].forEach((val, vidx) => {
                                    if(val != 'null' && val != 'NaN') {
                                        circles.push(<circle cx={widthScale(val)}
                                                            cy={heightScale(key)+band/2+jitter(0.8*band)}
                                                            r={3}
                                                            fillOpacity={0.7}
                                                            fill={color(key)}
                                                            key={kidx*data[key].length + vidx} />)
                                    }
                                });
                            });
                            if(this.props.activeDataset.loading) {
                                return <Loader />;
                            } else {
                                return (
                                    <svg width={width} height={width}>
                                        {circles}
                                        <XYAxis xScale={widthScale}
                                                yScale={heightScale}
                                                padding={padding}
                                                width={width}
                                                height={height}
                                                xlabel={"Value"}
                                                ylabel={"Columns"}/>
                                    </svg>);
                            }
                        }
                    }
                </ContainerDimensions>
            </div>
        );
    }
}

export default Dotplot;
