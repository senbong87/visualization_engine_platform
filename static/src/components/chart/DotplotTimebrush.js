import React from 'react';
import { connect } from 'react-redux';
import ContainerDimensions from 'react-container-dimensions';
import { bindActionCreators } from 'redux';
import * as d3 from 'd3';
import * as authActions from '../../actions/auth';
import * as datasetActions from '../../actions/dataset';
import { jsonListToColList, jitter } from '../../utils/misc';
import { Loader } from '../common';
import XYAxis from './geometry/XYAxis';

function mapStateToProps(state) {
    return {
        activeDataset: state.dataset.activeDataset,
        filteredDataset: state.dataset.filteredDataset,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Object.assign({}, authActions, datasetActions), dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
class DotplotTimebrush extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <ContainerDimensions>
                    { 
                        ({ width, height }) => {
                            if (this.props.activeDataset.rawData.length===0) {
                                return (<p>Please select a dataset.</p>)
                            }
                            const max_chars = this.props.activeDataset.selectedColumns.reduce(
                                                    (acc, x) => Math.max(acc, x.length), 8)
                            const padding = Math.min(max_chars*8, width/2.5);
                            const data = jsonListToColList(this.props.filteredDataset.rawData);

                            let numColumns = [];
                            let dateColumn = [];
                            let datetimeColNames = this.props.activeDataset.meta.columns.filter(x => x.type === 'datetime').map(x => x.name);
                            if (datetimeColNames.length===0) {
                                return (<p>Error: no time columns in the selected dataset.</p>);
                            }
                            this.props.activeDataset.selectedColumns.forEach((key) => {
                                // note: the last selected datetime column will be used
                                if (datetimeColNames.indexOf(key)>=0) {
                                    dateColumn = key;
                                } else {
                                    if(data[key].every(x => !isNaN(x))) {
                                        numColumns.push(key);
                                    }
                                }
                            });
                            if (dateColumn.length===0) {
                                return (<p>Please select a time column.</p>);
                            }
                            const datetimeCol = jsonListToColList(this.props.activeDataset.rawData)[dateColumn];

                            let minVal = d3.min(numColumns.map(key => d3.min(data[key])));
                            let maxVal = d3.max(numColumns.map(key => d3.max(data[key])));
                            minVal = typeof minVal !== 'undefined' ? minVal: 0;
                            maxVal = typeof maxVal !== 'undefined' ? maxVal: 1;
                            const heightBrush = 100;
                            const widthScale = d3.scaleLinear()
                                                 .range([padding, width - padding])
                                                 .domain([minVal, maxVal]);
                            const heightScale = d3.scaleBand()
                                                  .range([padding, height - padding-heightBrush])
                                                  .domain(numColumns);
                            const widthScaleBrush = d3.scaleTime()
                                                 .range([padding, width - padding])
                                                 .domain(d3.extent(datetimeCol));
                            const color = d3.scaleOrdinal(d3.schemeCategory10);
                            const band = heightScale.bandwidth();
                            let circles = [];
                            numColumns.forEach((key, kidx) => {
                                data[key].forEach((val, vidx) => {
                                    if(val != 'null' && val != 'NaN') {
                                        circles.push(<circle cx={widthScale(val)}
                                                            cy={heightScale(key)+band/2+jitter(0.8*band)}
                                                            r={3}
                                                            fillOpacity={0.7}
                                                            fill={color(key)}
                                                            key={kidx*data[key].length + vidx} />)
                                    }
                                });
                            });
                            //console.log(data);
                            let brush = d3.brushX()
                                            .extent([[0, 0], [width-2*padding, heightBrush-50]])
                                            .on("brush", () => {
                                                let s = d3.event.selection === null ? widthScaleBrush.range() : d3.event.selection;
                                                let s_inv = d3.event.selection.map(widthScaleBrush.invert);

                                                let filtered_output = this.props.activeDataset.rawData.filter(function(val) {
                                                    return (+val[dateColumn]>=+s_inv[0] && +val[dateColumn]<=+s_inv[1]);
                                                });
                                                // console.log(filtered_output);
                                                this.props.setFilteredDataset(filtered_output);
                                            });
                            
                            d3.selectAll(".brush").call(brush);
                            if(this.props.activeDataset.loading) {
                                return <Loader />;
                            } else {
                                return (
                                    <svg width={width} height={width}>
                                        {circles}
                                        <XYAxis xScale={widthScale}
                                                yScale={heightScale}
                                                padding={padding}
                                                width={width}
                                                height={height-heightBrush}
                                                xlabel={"Value"}
                                                ylabel={"Columns"}/>
                                        <XYAxis xScale={widthScaleBrush}
                                                yScale={heightScale}
                                                padding={padding}
                                                width={width}
                                                height={height}/>
                                        <rect   x={padding}
                                                y={height-padding-heightBrush+50}
                                                width={width-2*padding}
                                                height={heightBrush-50}
                                                style={{fill: '#ddd'}} />
                                        <g      className={"brush"}
                                                ref={"brushRect"} 
                                                transform={"translate("+padding+","+(height-heightBrush-padding+50)+")"}
                                                ></g>
                                    </svg>);
                            }
                        }
                    }
                </ContainerDimensions>
            </div>
        );
    }
}

export default DotplotTimebrush;
