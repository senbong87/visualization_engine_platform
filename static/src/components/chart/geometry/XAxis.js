import React from 'react';
import Axis from './axis';

export default (props) => {
    const xSettings = {
        translate: 'translate(0,' + (props.height - props.padding) + ')',
        scale: props.xScale,
        axisType: "axisBottom",
    };
    const xlabelTransform = "translate(" + (props.width/2) + "," + (props.height - props.padding/2) + ")";
    return (<g className="xy-axis">
                <Axis {...xSettings}/>
                <text transform={xlabelTransform}>{props.xlabel}</text>
            </g>);
}
