import React from 'react';
import Axis from './axis';
import * as d3 from 'd3';

export default (props) => {
    
    const ySettings = {
        translate: 'translate(' + props.xloc + ', 0)',
        scale: props.yScale,
        axisType: "axisLeft",
        brush: props.brush,
        xloc: props.xloc,
        yloc: props.yloc
    };
    d3.select("."+props.dim).call(props.brush);
    const ylabel_gap = -10;
    const labelTransform = "translate(" + (props.xloc) + "," + (props.yloc+ylabel_gap) + ")";
    return (<g className="p-axis">
                <Axis {...ySettings}/>
                <text transform={labelTransform} style={{textAnchor: "middle"}}>{props.label}</text>
                <g className={props.dim}	                 
	                transform={"translate("+(props.xloc)+","+(0)+")"}
	                ></g>
            </g>);
}
