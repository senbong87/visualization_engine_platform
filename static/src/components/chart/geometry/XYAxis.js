import React from 'react';
import Axis from './axis';

export default (props) => {
    const xSettings = {
        translate: 'translate(0,' + (props.height - props.padding) + ')',
        scale: props.xScale,
        axisType: "axisBottom",
    };
    const ySettings = {
        translate: 'translate(' + props.padding + ', 0)',
        scale: props.yScale,
        axisType: "axisLeft",
    };
    const xlabelTransform = "translate(" + (props.width/2) + "," + (props.height - props.padding/2) + ")";
    const ylabelTransform = "translate(" + (props.padding) + "," + (3*props.padding/4) + ")";
    return (<g className="xy-axis">
                <Axis {...xSettings}/>
                <Axis {...ySettings}/>
                <text transform={xlabelTransform}>{props.xlabel}</text>
                <text transform={ylabelTransform} style={{textAnchor: "middle"}}>{props.ylabel}</text>
            </g>);
}
