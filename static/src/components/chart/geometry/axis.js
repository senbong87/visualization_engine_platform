import React from 'react';
import PropTypes from 'prop-types';
import * as d3 from 'd3';

class Axis extends React.Component {
    static propTypes = {
        translate: PropTypes.string.isRequired,
        axisType: PropTypes.string.isRequired,
    };

    componentDidUpdate() {
        this.renderAxis();
    }

    componentDidMount() {
        this.renderAxis();
    }

    renderAxis() {
        var node = this.refs.axis;
        var axisMap = {
            axisBottom: d3.axisBottom().scale(this.props.scale),
            axisLeft: d3.axisLeft().scale(this.props.scale),
        };
        d3.select(this.refs.axis).call(axisMap[this.props.axisType]);
    }

    render() {
        return <g className="axis" ref="axis" transform={this.props.translate}></g>;
    }
}

export default Axis;
