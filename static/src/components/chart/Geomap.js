import React from 'react';
import { connect } from 'react-redux';
import ContainerDimensions from 'react-container-dimensions';
import { bindActionCreators } from 'redux';
import * as d3 from 'd3';
import * as authActions from '../../actions/auth';
import * as datasetActions from '../../actions/dataset';
import { jsonListToColList, jitter } from '../../utils/misc';
import { Loader } from '../common';
import XAxis from './geometry/XAxis';
import * as topojson from 'topojson';
import { COUNTRIES } from '../../constants';
import { AIRPORTS } from '../../constants';

function mapStateToProps(state) {
    return {
        activeDataset: state.dataset.activeDataset,
        filteredDataset: state.dataset.filteredDataset,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Object.assign({}, authActions, datasetActions), dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
class Geomap extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <ContainerDimensions>
                    { 
                        ({ width, height }) => {
                            if (this.props.activeDataset.rawData.length===0) {
                                return (<p>Please select a dataset.</p>)
                            }
                            const max_chars = this.props.activeDataset.selectedColumns.reduce(
                                                    (acc, x) => Math.max(acc, x.length), 8)
                            const padding = Math.min(max_chars*8, width/2.5);
                            
                            let numColumns = [];
                            let dateColumn = [];
                            let datetimeColNames = this.props.activeDataset.meta.columns.filter(x => x.type === 'datetime').map(x => x.name);
                            if (datetimeColNames.length === 0) {
                                return (<p>Error: no time columns in the selected dataset.</p>);
                            }
                            this.props.activeDataset.selectedColumns.forEach((key) => {
                                // note: the last selected datetime column will be used
                                if (datetimeColNames.indexOf(key)>=0) {
                                    dateColumn = key;
                                } else {
                                    numColumns.push(key);
                                }
                            });
                            if (dateColumn.length === 0) {
                                return (<p>Please select a time column.</p>);
                            }
                            let dataActive = jsonListToColList(this.props.activeDataset.rawData);
                            const datetimeCol = dataActive[dateColumn];
                            dataActive = null;

                            // geomap
                            let mapwidth = width, mapheight = 620; // svg width=920
                            const countries_feature = topojson.feature(COUNTRIES, COUNTRIES.objects.countries).features;
                            const airports_feature = topojson.feature(AIRPORTS, AIRPORTS.objects.airports).features;

                            let projection = d3.geoMercator()
                                             .scale(147)
                                             .translate([mapwidth / 2, mapheight / 1.41]);
                            let path = d3.geoPath()
                                       .pointRadius(2)
                                       .projection(projection);
                            let geomapCountry = <g className='countries'>
                                            {
                                                countries_feature.map((d, i) => (
                                                    <path
                                                        key={ `country-${ i }` }
                                                        d={ path(d) }
                                                        fill="#b0d0ab"
                                                        stroke="#6cb0e0"
                                                        strokeWidth={0.5}
                                                    />
                                                ))
                                            }
                                        </g>;
                            let geomapAirport = <g className="airports">
                                            {
                                                airports_feature.map((d, i) => (
                                                    <path
                                                        key={ `airport-${ i }` }
                                                        d={ path(d) }
                                                        fill="#036"
                                                        stroke="#6cb0e0"
                                                        strokeWidth={0.5}
                                                    />
                                                ))
                                            }
                                        </g>;

                            let airportMap = {};
                            airports_feature.map((d) => {
                                airportMap[d.properties.iata_code] = d.geometry.coordinates;
                            });
                            let geomapRoute = [];
                            let errorinfo = null;
                            if (numColumns.indexOf('origin')>=0 && numColumns.indexOf('destination')>=0) {
                                geomapRoute = <g className='routes'>                                              
                                                {
                                                    this.props.filteredDataset.rawData.map((d, i) => (
                                                        <path
                                                            key={ `route-${ i }` }
                                                            d={ path({"type": "LineString", "coordinates": [airportMap[d.origin], airportMap[d.destination]]}) }
                                                            fill='none'
                                                            stroke="white"
                                                        />
                                                    ))
                                                }
                                            </g>
                            } else {
                                console.log("No origin and/or destination column(s).");
                                errorinfo = <text x="5" y="15">No origin and/or destination column(s) selected.</text>;
                            }

                            // brush
                            const heightBrush = 100;
                            const widthScaleBrush = d3.scaleTime()
                                                 .range([padding, width - padding])
                                                 .domain(d3.extent(datetimeCol));
                            let brush = d3.brushX()
                                            .extent([[0, 0], [width-2*padding, heightBrush-50]])
                                            .on("brush", () => {
                                                let s = d3.event.selection === null ? widthScaleBrush.range() : d3.event.selection;
                                                let s_inv = d3.event.selection.map(widthScaleBrush.invert);

                                                let filtered_output = this.props.activeDataset.rawData.filter(function(val) {
                                                    return (+val[dateColumn]>=+s_inv[0] && +val[dateColumn]<=+s_inv[1]);
                                                });
                                                this.props.setFilteredDataset(filtered_output);
                                            });
                            d3.selectAll(".brush").call(brush);                            

                            if(this.props.activeDataset.loading) {
                                return <Loader />;
                            } else {
                                return (
                                    <svg width={width} height={width}>
                                        <rect   width={width}
                                                height={mapheight}
                                                className="geomapBkg"
                                                fill='#87CEEB' />
                                        {errorinfo}
                                        {geomapCountry}
                                        {geomapAirport}
                                        {geomapRoute}
                                        <XAxis  xScale={widthScaleBrush}
                                                padding={padding}
                                                width={width}
                                                height={height}/>
                                        <rect   x={padding}
                                                y={height-padding-heightBrush+50}
                                                width={width-2*padding}
                                                height={heightBrush-50}
                                                style={{fill: '#ddd'}} />
                                        <g      className={"brush"}
                                                ref={"brushRect"} 
                                                transform={"translate("+padding+","+(height-heightBrush-padding+50)+")"}
                                                ></g>
                                    </svg>
                                );
                            }
                        }
                    }
                </ContainerDimensions>
            </div>
        );
    }
}

export default Geomap;
