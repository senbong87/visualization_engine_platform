import React from 'react';
import { connect } from 'react-redux';
import ContainerDimensions from 'react-container-dimensions';
import { bindActionCreators } from 'redux';
import * as d3 from 'd3';
import * as authActions from '../../actions/auth';
import * as datasetActions from '../../actions/dataset';
import { jsonListToColList } from '../../utils/misc';
import { Loader } from '../common';
import XYAxis from './geometry/XYAxis';

function mapStateToProps(state) {
    return {
        activeDataset: state.dataset.activeDataset,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Object.assign({}, authActions, datasetActions), dispatch);
}


@connect(mapStateToProps, mapDispatchToProps)
class TimeSeries extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <ContainerDimensions>
                    { 
                        ({ width, height }) => {
                            if (this.props.activeDataset.rawData.length===0) {
                                return (<p>Please select a dataset.</p>)
                            }
                            const max_chars = this.props.activeDataset.selectedColumns.reduce(
                                                    (acc, x) => Math.max(acc, x.length), 8)
                            const padding = Math.min(max_chars*8, width/2.5);
                            const data = jsonListToColList(this.props.activeDataset.rawData);

                            let numColumns = [];
                            let dateColumn = [];
                            const datetimeColNames = this.props.activeDataset.meta.columns.filter(x => x.type === 'datetime').map(x => x.name);
                            if (datetimeColNames.length===0) {
                                return (<p>Error: no time columns in the selected dataset.</p>);
                            }
                            this.props.activeDataset.selectedColumns.forEach((key) => {
                                // note: the last selected datetime column will be used
                                if (datetimeColNames.indexOf(key)>=0) {
                                    dateColumn = key;
                                } else {
                                    if(data[key].every(x => !isNaN(x))) {
                                        numColumns.push(key);
                                    }
                                }
                            });
                            if (dateColumn.length===0) {
                                return (<p>Please select a time column.</p>);
                            }

                            let minVal = d3.min(numColumns.map(key => d3.min(data[key])));
                            let maxVal = d3.max(numColumns.map(key => d3.max(data[key])));
                            minVal = typeof minVal !== 'undefined' ? minVal: 0;
                            maxVal = typeof maxVal !== 'undefined' ? maxVal: 1;
                            const widthScale = d3.scaleTime()
                                                 .range([padding, width - padding])
                                                 .domain(d3.extent(data[dateColumn]));
                            const heightScale = d3.scaleLinear()
                                                  .range([height - padding, padding])
                                                  .domain([minVal, maxVal]);
                            const color = d3.scaleOrdinal(d3.schemeCategory10);
                            let circles = [];
                            numColumns.forEach((key, kidx) => {
                                data[key].forEach((val, vidx) => {
                                    if(val != 'null' && val != 'NaN') {
                                        circles.push(<circle cx={widthScale(data[dateColumn][vidx])}
                                                            cy={heightScale(val)}
                                                            r={3}
                                                            fillOpacity={0.7}
                                                            fill={color(key)}
                                                            key={kidx*data[key].length + vidx} />)
                                    }
                                });
                            });

                            // legend (todo: reusable component?)
                            const legendPosition = {top: 40, left: width-130, width: 20, height: 20};
                            let legend = [];
                            numColumns.forEach( (colname, idx) => {
                                legend.push(
                                    <circle cx={legendPosition.left}
                                            cy={legendPosition.top + idx*legendPosition.height}
                                            r={5}
                                            fillOpacity={0.7}
                                            fill={color(colname)}
                                            key={numColumns.length*data[colname].length + idx*2+1} />
                                );
                                legend.push(
                                    <text x={legendPosition.left+legendPosition.width}
                                            y={legendPosition.top + idx*legendPosition.height}
                                            dominantBaseline='central'
                                            key={numColumns.length*data[colname].length + idx*2+2}>
                                        {colname}
                                    </text>
                                );

                            });

                            if(this.props.activeDataset.loading) {
                                return <Loader />;
                            } else {
                                return (
                                    <svg width={width} height={width}>
                                        {circles}
                                        <XYAxis xScale={widthScale}
                                                yScale={heightScale}
                                                padding={padding}
                                                width={width}
                                                height={height}
                                                xlabel={"Time"}
                                                ylabel={"Value"}/>
                                        {legend}
                                    </svg>);
                            }
                        }
                    }
                </ContainerDimensions>
            </div>
        );
    }
}

export default TimeSeries;
