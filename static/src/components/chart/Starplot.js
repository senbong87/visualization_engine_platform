import React from 'react';
import { connect } from 'react-redux';
import ContainerDimensions from 'react-container-dimensions';
import { bindActionCreators } from 'redux';
import * as d3 from 'd3';
import * as authActions from '../../actions/auth';
import * as datasetActions from '../../actions/dataset';
import { jsonListToColList, jitter } from '../../utils/misc';
import { Loader } from '../common';
import XAxis from './geometry/XAxis';

function mapStateToProps(state) {
    return {
        activeDataset: state.dataset.activeDataset,
        filteredDataset: state.dataset.filteredDataset,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Object.assign({}, authActions, datasetActions), dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
class Starplot extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <ContainerDimensions>
                    { 
                        ({ width, height }) => {
                            if (this.props.activeDataset.rawData.length===0) {
                                return (<p>Please select a dataset.</p>)
                            }
                            const max_chars = this.props.activeDataset.selectedColumns.reduce(
                                                    (acc, x) => Math.max(acc, x.length), 8)
                            const padding = Math.min(max_chars*8, width/2.5);
                            const data = jsonListToColList(this.props.filteredDataset.rawData);

                            let numColumns = [];
                            let dateColumn = [];
                            let datetimeColNames = this.props.activeDataset.meta.columns.filter(x => x.type === 'datetime').map(x => x.name);
                            if (datetimeColNames.length===0) {
                                return (<p>Error: no time columns in the selected dataset.</p>);
                            }
                            this.props.activeDataset.selectedColumns.forEach((key) => {
                                // note: the last selected datetime column will be used
                                if (datetimeColNames.indexOf(key)>=0) {
                                    dateColumn = key;
                                } else {
                                    if(data[key].every(x => !isNaN(x))) {
                                        numColumns.push(key);
                                    }
                                }
                            });
                            if (dateColumn.length===0) {
                                return (<p>Please select a time column.</p>);
                            }
                            let dataActive = jsonListToColList(this.props.activeDataset.rawData);
                            const datetimeCol = dataActive[dateColumn];
                            const maxVals = numColumns.map(key => d3.max(dataActive[key]));
                            const minVals = numColumns.map(key => d3.min(dataActive[key]));
                            dataActive = null;

                            // brush
                            const heightBrush = 100;
                            const widthScaleBrush = d3.scaleTime()
                                                 .range([padding, width - padding])
                                                 .domain(d3.extent(datetimeCol));
                            let brush = d3.brushX()
                                            .extent([[0, 0], [width-2*padding, heightBrush-50]])
                                            .on("brush", () => {
                                                let s = d3.event.selection === null ? widthScaleBrush.range() : d3.event.selection;
                                                let s_inv = d3.event.selection.map(widthScaleBrush.invert);

                                                let filtered_output = this.props.activeDataset.rawData.filter(function(val) {
                                                    return (+val[dateColumn]>=+s_inv[0] && +val[dateColumn]<=+s_inv[1]);
                                                });
                                                this.props.setFilteredDataset(filtered_output);
                                            });
                            d3.selectAll(".brush").call(brush);

                            // star plot
                            const bkglevels = 5,
                                    starPadding = 50,
                                    starHeight = height - heightBrush - starPadding*2,
                                    starWidth = width - starPadding*2,
                                    radius = Math.min(starWidth/2, starHeight/2),
                                    angle = 2*Math.PI/numColumns.length;
                            let starBkgLines = [],
                                starAxis = [],
                                starLines = [],
                                starDots = [];
                            numColumns.forEach((key, kidx) => {
                                for (let j=0; j<bkglevels-1; j++) {
                                    let levelFactor = radius*((j+2)/bkglevels);
                                    starBkgLines.push(<line x1={levelFactor*(1-Math.sin(kidx*angle))}
                                                            y1={levelFactor*(1-Math.cos(kidx*angle))}
                                                            x2={levelFactor*(1-Math.sin((kidx+1)*angle))}
                                                            y2={levelFactor*(1-Math.cos((kidx+1)*angle))}
                                                            className={'line'}
                                                            transform={"translate("+(starWidth/2+starPadding-levelFactor)+","+(starHeight/2+starPadding-levelFactor)+")"}
                                                            stroke={'grey'}
                                                            strokeOpacity={0.8}
                                                            strokeWidth={'0.3px'}
                                                            key={kidx*bkglevels+j}></line>);
                                    // TODO: tick texts?
                                }
                                starAxis.push(<g    className={"axis"}
                                                    transform={"translate("+(starWidth/2-radius+starPadding)+","+(starHeight/2-radius+starPadding)+")"}>
                                                <line   x1={radius}
                                                        y1={radius}
                                                        x2={radius*(1-Math.sin(kidx*angle))}
                                                        y2={radius*(1-Math.cos(kidx*angle))}
                                                        className={'line'}
                                                        stroke={'grey'}
                                                        strokeWidth={1+'px'}
                                                        key={numColumns.length*bkglevels+kidx}></line>
                                                <text   x={radius*(1-Math.sin(kidx*angle))-60*Math.sin(kidx*angle)}
                                                        y={radius*(1-Math.cos(kidx*angle))-20*Math.cos(kidx*angle)}
                                                        className={'legend'}
                                                        transform={'translate(0,'+(-0)+')'}
                                                        fontFamily={'sans-serif'}
                                                        fontSize={'15px'}
                                                        textAnchor={"middle"}
                                                        // dy={'1.5em'}
                                                        key={numColumns.length*bkglevels*2+kidx}>{key}</text>
                                            </g>);
                            });
                            
                            const smallNum = 0.00000001,
                                    color = d3.scaleOrdinal(d3.schemeCategory10);
                            let xCoord = [],
                                yCoord = [];
                            this.props.filteredDataset.rawData.forEach((val, vidx) => {
                                let pointsCoords = "";
                                numColumns.forEach((key, kidx) => {
                                    if(val[key] != 'null' && val[key] != 'NaN') {
                                        // debugger;
                                        xCoord = radius*(1-((val[key]-minVals[kidx])/(maxVals[kidx]-minVals[kidx]+smallNum))*Math.sin(kidx*angle));
                                        yCoord = radius*(1-((val[key]-minVals[kidx])/(maxVals[kidx]-minVals[kidx]+smallNum))*Math.cos(kidx*angle));
                                        starDots.push(<circle   cx={xCoord}
                                                                cy={yCoord}
                                                                r={4}
                                                                fill={color(1)}
                                                                fillOpacity={0.85}
                                                                className={key}
                                                                transform={"translate("+(starWidth/2-radius+starPadding)+","+(starHeight/2-radius+starPadding)+")"}
                                                                key={numColumns.length*bkglevels*3+ vidx*numColumns.length+kidx} />);
                                        pointsCoords = pointsCoords.concat(xCoord + "," + yCoord + " ");
                                    }
                                });
                                starLines.push(<polygon     points={pointsCoords}
                                                            strokeWidth={"1px"}
                                                            stroke={color(1)}
                                                            fill={color(1)}
                                                            fillOpacity={0.1}
                                                            transform={"translate("+(starWidth/2-radius+starPadding)+","+(starHeight/2-radius+starPadding)+")"}
                                                            key={numColumns.length*bkglevels*3+this.props.filteredDataset.rawData.length*numColumns.length+vidx} />);
                            });

                            if(this.props.activeDataset.loading) {
                                return <Loader />;
                            } else {
                                return (
                                    <svg width={width} height={width}>
                                        // starplot DOM
                                        {starBkgLines}
                                        {starAxis}
                                        {starLines}
                                        {starDots}
                                        // brush DOM
                                        <XAxis  xScale={widthScaleBrush}
                                                padding={padding}
                                                width={width}
                                                height={height}/>
                                        <rect   x={padding}
                                                y={height-padding-heightBrush+50}
                                                width={width-2*padding}
                                                height={heightBrush-50}
                                                style={{fill: '#ddd'}} />
                                        <g      className={"brush"}
                                                ref={"brushRect"} 
                                                transform={"translate("+padding+","+(height-heightBrush-padding+50)+")"}
                                                ></g>
                                    </svg>);
                            }
                        }
                    }
                </ContainerDimensions>
            </div>
        );
    }
}

export default Starplot;
