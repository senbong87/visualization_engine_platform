import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as authActions from '../actions/auth';
import * as datasetActions from '../actions/dataset';
import { trunc } from '../utils/misc';
import { Loader } from './common';

function mapStateToProps(state) {
    return {
        token: state.auth.token,
        datasetsList: state.dataset.datasetsList,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Object.assign({}, authActions, datasetActions), dispatch);
}

function mergeProps(stateProps, dispatchProps) {
    return {
        ...stateProps,
        ...dispatchProps,
        fetchDatasets: () => {
            const token = stateProps.token;
            dispatchProps.fetchProtectedDatasets(token).payload.then(
                (response) => {
                    !response.error 
                        ? dispatchProps.fetchProtectedDatasetsSuccess(response.data)
                        : dispatchProps.fetchProtectedDatasetsFailure(response.error)
                }
            );
        },
        deleteDataset: (id) => {
            const token = stateProps.token;
            dispatchProps.deleteProtectedDataset(token, id).payload.then(
                (response) => {
                    !response.error
                        ? dispatchProps.deleteProtectedDatasetSuccess(response.result)
                        : dispatchProps.deleteProtectedDatasetFailure(response.error)
                }
            );
        },
        createDataset: (file) => {
            const token = stateProps.token;
            dispatchProps.createProtectedDataset(token, file).payload.then(
                (response) => {
                    !response.error
                        ? dispatchProps.createProtectedDatasetSuccess(response.result)
                        : dispatchProps.createProtectedDatasetFailure(response.error)
                }
            );
        },
    }
}

class Dataset extends React.Component {
    render() {
        let created_at = new Date(this.props.created_at);
        let num_cols = this.props.columns.filter(x => x.type === 'numerical').map(x => trunc(x.name, 15));
        let cat_cols = this.props.columns.filter(x => x.type === 'categorical').map(x => trunc(x.name, 15));
        let date_cols = this.props.columns.filter(x => x.type == 'datetime').map(x => trunc(x.name, 15));
        return (
            <div className="col-md-3 col-sm-4 col-xs-6">
				<div className="panel panel-info">
                    <div className="panel-heading">
                        {trunc(this.props.filename, 20)}
                        <span
                            className="glyphicon glyphicon-remove pull-right"
                            id={this.props.id}
                            onClick={this.props.handleRemoveIconClick}>
                        </span>
                    </div>
                    <div className="panel-body">
                        <p><span className="text-primary">shape:</span> ({this.props.row_num}, {this.props.col_num})</p>
                        {cat_cols.length == 0? null:
                            <p><span className="text-primary">categorical columns:</span> {trunc(cat_cols.join(', '), 100)}</p>
                        }
                        {num_cols.length == 0? null:
                            <p><span className="text-primary">numerical columns:</span> {trunc(num_cols.join(', '), 100)}</p>
                        }
                        {date_cols.length == 0? null:
                            <p><span className="text-primary">datetime columns:</span> {trunc(date_cols.join(', '), 100)}</p>
                        }
                        <p><span className="text-primary">upload date:</span> {created_at.toLocaleDateString()}</p>
                        <p><span className="text-primary">upload time:</span> {created_at.toLocaleTimeString()}</p>
                    </div>
				</div>
            </div>
        );
    }
}

class DatasetsList extends React.Component {
    render() {
        const datasets = this.props.datasets.map((dataset) => (
            <Dataset
                key={dataset.id}
                id={dataset.id}
                created_at={dataset.created_at}
                filename={dataset.filename}
                row_num={dataset.row_num}
                col_num={dataset.col_num}
                columns={dataset.columns}
                handleRemoveIconClick={() => this.props.handleRemoveIconClick(dataset.id)} />
        ));
        return (
            <div>{datasets}</div>
        );
    }
}

class AddButton extends React.Component {
    render() {
        return (
            <div className="col-md-2 col-sm-4 col-xs-6 text-center">
                <label className="btn btn-default btn-circle" style={{marginTop:'45px'}}>
                    <span className="glyphicon glyphicon-plus"></span>
                    <input type="file"
                        hidden={true}
                        onChange={(evt) => this.props.handleFileChange(evt)} />
                </label>
            </div>
        );
    }
}

@connect(mapStateToProps, mapDispatchToProps, mergeProps)
class DatasetsDashboard extends React.Component { // eslint-disable-line react/prefer-stateless-function
    componentDidMount() {
        this.props.fetchDatasets();
    }

    render() {
        const content = (
            <div className="row">
                <DatasetsList
                    datasets={this.props.datasetsList.datasets}
                    handleRemoveIconClick={
                        (id) => {
                            this.props.deleteDataset(id);
                            this.props.fetchDatasets();
                        }
                    } />
                <AddButton
                    handleFileChange={
                        (evt) => {
                            this.props.createDataset(evt.target.files[0]);
                            this.props.fetchDatasets();
                        }
                    } />
            </div>
        );
        return (
            <div className="col-md-12">
                <h1>Datasets</h1>
                <br />
                {
                    !this.props.datasetsList.loading
                        ? content
                        : <Loader />
                }
            </div>
        );
    }
}

export default DatasetsDashboard;
